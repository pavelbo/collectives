# Willkommen in eurem neuen Kollektiv

**Kommt vorbei, organisiert euch und teilt euer Wissen!**


### 👥 Ladet weitere Personen ins Kollektiv ein

Egal ob ihr Freund:innen oder ganze Gruppen zufügen wollt, Mitglieder könnt ihr in der [Circles-App](/index.php/apps/circles/) verwalten.

### 🌱 Füllt euer Kollektiv mit Leben

Erstellt Seiten und teilt eure wertvollen Gedanken. Ob als gemeinsame Wissensablage der Community oder Handbuch für eure Organisation — mit einem Kollektiv klappt das!

### 🛋️ Verändert die Startseite und fühlt euch wie Zuhause

Wechselt in den Editier-Modus, um loszulegen! ↗️


## Auch gut zu wissen

* Verlinkt lokale Seiten, indem ihr Text markiert und "Datei verknüpfen" wählt.
* Mehrere Menschen können gleichzeitig die selbe Seite bearbeiten.
* Findet mehr über diese App heraus und schaut in die [Dokumentation](https://collectivecloud.gitlab.io/collectives/).
* Fragt [die Community](https://help.nextcloud.com/c/apps/collectives/174) um Hilfe, wenn ihr weitere Fragen habt.
