# Welcome to your new collective

**Come, organize and build shared knowledge!**


### 👥 Invite new people to the collective

Whether you want to add your friends or whole groups, head over to [the Circles app](/index.php/apps/circles/) to add new members.

### 🌱 Bring life to your collective

Create pages and share your thoughts that really matter. Whether it's a shared knowledge repository for the community or a handbook for your organisation – Collectives works!

### 🛋️ Edit this landing page to feel like home

Push the pencil button to get started! ↗️


## Also good to know

* Link local pages by selecting text and choosing "link file".
* Multiple people can edit the same page simultaneously.
* Find out more about this App in the [documentation](https://collectivecloud.gitlab.io/collectives/).
* Ask [the community](https://help.nextcloud.com/c/apps/collectives/174) for help in case of questions.
