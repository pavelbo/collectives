# Velkommen til din nye gemenskap

*Ta del, organiser og bygg delt kunnskap!*


### 🐾 Legg til dine venner i kollektivet

Medlemmer kan håndteres i [Sirkelprogrammet](/index.php/apps/circles/).

### 🌱 Gi liv til din gemenskap

Opprett sider og del kunnskap som betyr noe.

### 🛋️ Rediger denne landingssiden for å gjøre ting innbydende

Trykk på blyanten for å begynne. ↗️


## Også nyttig å vite

* Lenk sammen lokale sider ved først å velge tekst, og så «lenk fil».
* Flere kan redigere samme side samtidig.
* Finn ut mer om dette programmet i [dokumentasjonen](https://collectivecloud.gitlab.io/collectives/).
* Spør [gemenskapen](https://help.nextcloud.com/c/apps/collectives/174) om hjelp i fall du har problemer.
